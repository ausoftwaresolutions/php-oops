<?php
/*
=> What is Access Modifier?

	==	Access Modifier allows you to alter the visibility of any class member(properties and method).

		In php there are three scopes for class members.

		Public
		Protected and
		Private
	
		PUBLIC:
			Those class properties and class methods which are set to be PUBLIC is accessed from any where in PHP script.
		
		PRIVATE:
			Those Class properties and class methods which are set to be PRIVATE can only be access with in the class.	
			
		PROTECTED:
			Those class properties and class methods which are set to be PROTECTED can only be accessed in side a class and from its child class.


	
	
*/


<?php
 
// PUBLIC 
class Car
{
	public $model;    // public methods and properties.
	public function getModel()
	{
		return "The car model is " . $this -> model;
	}
}
 
$mercedes = new Car(); //Here we access a property from outside the class
$mercedes -> model = "Mercedes"; //Here we access a method from outside the class
echo $mercedes -> getModel();

echo "\n\n";

// PRIVATE


 
?>
