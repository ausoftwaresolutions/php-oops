<?php
/*
=> What is Encapsulation?

	==	Wrapping some data in single unit is called Encapsulation. Encapsulation is used to safe data or information in an object from other it means encapsulation is mainly used for protection purpose.
	
	
	class Bag{
		pen(); // calling function of pen
		readBook(); //calling function of read book
	}
*/

class arithmetic
{
	var $first=1000;
	var $second=500;
 
	function add()
	{
		$res=$this->first+$this->second;
		echo "Addition = ".$res;
	}
	 
	function sub()
	{
		$res=$this->first-$this->second;
		echo "Subtraction = ".$res;
	}
	 
	function mult()
	{
		$res=$this->first*$this->second;
		echo "Multiplication = ".$res;
	}
	 
	function div()
	{
		$res=$this->first/$this->second;
		echo "Division = ".$res;
	}
	 
}
 
$obj= new arithmetic( );
$obj->add(); echo "\n";
$obj->sub(); echo "\n";
$obj->mult(); echo "\n";
$obj->div(); echo "\n";
 
?>
