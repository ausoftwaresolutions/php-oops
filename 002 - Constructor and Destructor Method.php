<?php
// 002 - Constructor and Destructor Method
/*
=> What is the Constructor Method in PHP?
	
	==	The method that automatically executes the first time when creating a new object from class, it’s called constructor method in PHP. Constructor Method is predefined Public Method. To declare any constructor, we have to write two times the underscore (_) before the construct word after the public function. That’s mean we have to declare any constructor like this: public function __construct().
	
	
=> What is the Destructor Method in PHP?

	==	When the object has finished its job, then the method in the class which is automatically executed or execute at the end, that method is called Destructor Method in PHP. Destructor Method is predefined Public Method. To declare any destructor, we have to write two times the underscore (_) before the destruct word after the public function.
	
*/

class SampleClass
{
	public function __construct()
	{
		$xskc = "";
		echo "\n Hii, I am Constructor \n";
	}
	
	public function others()
	{
		echo "\n Hii, I am other function of this class \n";
	}
	
	public function __destruct()
	{
		echo "\n Hii, I am Destructor \n";
	}
	
	
}

$NewObj1 = new SampleClass(); // Now Automatically Execute __construct Method and Will show "Hii, I am Constructor" First.
$NewObj1->others();

?>