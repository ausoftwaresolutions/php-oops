<?php
class ClassExample
{
	function add($a, $b)
	{
		return $a + $b;
	}
	
	function addInArray($string, $array)
	{
		return $array[] = $string;
	}
}

$obj = new ClassExample();

// Calling the function to add the elements into array
$add_array[] = $obj->addInArray("Udit", $newArray = []); // In this line, we defined the array in one line or parameter 
$add_array[] = $obj->addInArray("Kumar", $newArray = []);
print_r($add_array);
echo "\n";


// Now we will call "add" function
// We have 2 numbers
$a = 5;
$b = 6;
$addNumbers = $obj->add($a, $b);
echo "The SUM of ".$a." and ".$b." is ".$addNumbers;
echo "\n";
/*
=> Introduction to Object Oriented Programming:

	==	To understand object oriented programming, we first need to know what is object? And what is class?
	
	
=> What is Object?

	==	Everything we see around us is object. Suppose you look at yourself and your surroundings, your coat, your pants, mobile, 	tables, chairs, computers all these are objects. Even you and I myself are an object.


=> What is Class?
	
	==	Now we know about object. Let’s know about class. A class is the replica of an object or the blueprint of an object. Suppose we want to make a chair. At the beginning, we can not make a chair without any thought. We plan for it – what will be the chair looks like, how much will it be in length, width and height etc. These plans are what we write somewhere as document. Our document is actually a class. To explain a little bit more, we can say that the name of the object is determined by the name of the class. The object created by a class is called an instance of that class.
	
	
=> What is Object Oriented Programming?

	==	Now the question is, what is Object Oriented Programming? Object Oriented Programming is a computer programming which is defined by using the concept of class and object is called Object Oriented Programming. Object Orient theory is founded by three concepts:

		Inheritance
		Encapsulation
		Polymorphism



*/
?>