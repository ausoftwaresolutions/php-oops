<?php
/*
=> What is Method Chaining in PHP?

	== When many methods are called in a single instruction, in PHP’s term it is called Method Chaining.

Let’s explain it a little easier, suppose you have a class named Person, where you have two private property named $name and $designation. And there are two methods named setName() and setDesign() to set value in $name and $designation property. And lastly, to show the value of $name and $designation property, there is a getResult() method.


“Getters” and “Setters” are object methods that allow you to control access to a certain class variables / properties. Sometimes, these functions are referred to as “mutator methods”. A “getter” allows to you to retrieve or “get” a given property. A “setter” allows you to “set” the value of a given property.

*/

class Person
{
	private $name = '';
	private $designation = '';
	
	public function setName($input_name = '') // Setter
	{
		$this->name = $input_name;
	}
	
	public function setDesign($inp_desig = '') // Setter
	{
		$this->designation = $inp_desig;
	}
	
	public function getResult() // Getter
	{
		echo "Hello, My name is ".$this->name." and my designation is ".$this->designation;
	}
}

$person_info = new Person();
$person_info->setName("Udit");
$person_info->setDesign("Software Developer");

echo "\n";
$person_info->getResult();
echo "\n";
?>