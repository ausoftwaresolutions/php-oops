<?php
/*
=> What is Inheritance?

	==	With inheritance we can create child classes that are based on the parent class. A child class inherits all the properties and methods of its parent, and it can also add additional properties and methods. To create a child class that ' s based on a parent class, you use the extends keyword.
*/


class Person
{
	protected $name = '';
	protected $designation = '';
	
	public function setName($input_name = '') // Setter
	{
		$this->name = $input_name;
	}
	
	public function setDesign($inp_desig = '') // Setter
	{
		$this->designation = $inp_desig;
	}
	
	public function getResult() // Getter
	{
		echo "Hello, My name is ".$this->name." and my designation is ".$this->designation;
	}
}

class ExtraRole extends Person
{
	protected $SpecialTaskAssigned = '';
	
	public function setTask($input_task = '')
	{
		$this->SpecialTaskAssigned = $input_task;
	}
	
	public function getResult2()
	{
		echo "Hello, My name is ".$this->name." and my designation is ".$this->designation.". My another task is ".$this->SpecialTaskAssigned;
	}
}

echo "\n\t Without Inheritannce\n";
$person_info = new Person();
$person_info->setName("Udit");
$person_info->setDesign("Software Developer");
echo "\n";
$person_info->getResult();
echo "\n";

echo "\n\t With Inheritannce\n";
$person_info2 = new ExtraRole();
$person_info2->setName("Udit");
$person_info2->setDesign("Software Developer");
$person_info2->setTask("Project Management");

echo "\n";
$person_info2->getResult2();
echo "\n";



?>