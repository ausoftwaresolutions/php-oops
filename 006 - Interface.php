<?php
/*
=> What is Interface?

	==	An Interface is a class with functions without defination. Interfaces are defined in the same way as a class, but with the interface keyword replacing the class keyword and without any of the methods having their contents defined.
*/

interface Person
{
	public function setName();
	public function setDesign();
	public function getResult();
}

interface Salary
{
	public function setMonthlySalary();
}


class PersonInfo implements Person, Salary // multi level inheritance
{
	protected $name = '';
	protected $designation = '';
	protected $Salary = '';
	
	public function setName($input_name = '') // Setter
	{
		$this->name = $input_name;
	}
	
	public function setDesign($inp_desig = '') // Setter
	{
		$this->designation = $inp_desig;
		
	}
	
	public function setMonthlySalary($inp_sal = '')
	{
		$this->Salary = $inp_sal;
	}
	
	public function getResult() // Getter
	{
		echo "Hello, My name is ".$this->name." and my designation is ".$this->designation.". My monthely salary is ".$this->Salary;
	}
}

echo "\n";
$person_info = new PersonInfo();
$person_info->setName("abcd");
$person_info->setDesign("Software Developer");
$person_info->setDesign("15000");
echo "\n";
$person_info->getResult();
echo "\n";

?>