<?php
/*
=> What is Abstract Class?

	==	An abstract class is a class that is only partially implemented by the programmer. It may contain one or more abstract methods. An abstract method is simply a function definition that serves to tell the programmer that the method must be implemented in a child class.
*/


abstract class Salary
{
	protected $monthly_salary = '';
	
	abstract public function salary_annual();
	
	public function setSalary($salary = '') // Setter
	{
		$this->monthly_salary = $salary;
	}
	
}

class EmployeeInfo extends Salary
{
	protected $name = '';
	protected $designation = '';
	protected $annualSalary = '';
	
	
	public function setName($input_name = '') // Setter
	{
		$this->name = $input_name;
	}
	
	public function setDesign($inp_desig = '') // Setter
	{
		$this->designation = $inp_desig;
	}
	
	public function salary_annual()
	{
		$this->annualSalary = $this->monthly_salary*12;
	}
	
	public function getResult() // Getter
	{
		echo "Hello, My name is ".$this->name." and my designation is ".$this->designation.". My Anual salary is ".$this->annualSalary;
	}
}

echo "\n";
$person_info = new EmployeeInfo();
$person_info->setName("ABCD");
$person_info->setDesign("Software Developer");
$person_info->setSalary("15000");
$person_info->salary_annual();
echo "\n";
$person_info->getResult();
echo "\n";

?>