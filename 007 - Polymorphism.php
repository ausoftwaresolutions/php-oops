<?php
/*
=> What is Polymorphism?

	==	According to the Polymorphism principle, methods in different classes that do similar things should have the same name.
*/

interface Shape
{
	public function calcArea();
}

class Circle implements Shape
{
	private $radius;
	
	public function __construct($radius)
	{
		$this->radius = $radius;
	}
	public function calcArea()
	{
		$r = $this->radius;
		return pi()*pow($r, 2);
	}
}

class Rectangle implements Shape
{
	private $width;
	private $height;
	
	public function __construct($width, $height)
	{
		$this->width	= $width;
		$this->height	= $height;
	}
	
	public function calcArea()
	{
		return $this->width*$this->height;
	}
}
$circ = new Circle(3);
$rect = new Rectangle(3,5);

echo $circ->calcArea();
echo "\n";
echo $rect->calcArea();
echo "\n\n";

?>